export enum Type {
  G = 'G', // group
  GROUP = 'GROUP', // group
  GROUPEND = 'GROUPEND', // groupEnd
  GE = 'GROUPEND', // groupEnd
  L = 'L', // log
  LOG = 'LOG', // log
  I = 'I', // info
  INFO = 'INFO', // info
  E = 'E', // error
  ERROR = 'ERROR', // error
  D = 'D', // dev
  DEV = 'DEV', // dev
  JSON = 'JSON', // json
}

export enum TypeConsole {
  G = 'group', // group
  GROUP = 'group', // group
  GROUPEND = 'groupEnd', // groupEnd
  L = 'log', // log
  LOG = 'log', // log
  I = 'info', // info
  INFO = 'info', // info
  E = 'error', // error
  ERROR = 'error', // error
}
