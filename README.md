### camel-logger - NodeJS file logger

[![npm version](https://badge.fury.io/js/camel-logger.svg)](https://badge.fury.io/js/camel-logger)
![NPM](https://img.shields.io/npm/l/camel-logger?logo=npm)

### What New Version 2.0.0

- The module has been rewritten in TypeScript

### Install:

```bash
npm i camel-logger
```

### Just require it

```javascript
const { Logger } = require('camel-logger');
const logger = new Logger('log/log_file.txt'); // Log file directory
```

### Main function

```javascript
await logger.group('Group log');
await logger.line();
await logger.log('Log .log');
await logger.text('Log .text');
await logger.delimiter();
await logger.groupEnd('Group log');
// logger.groupend('Group log') // alias

// ----- [GROUP]: Group log -----
// [2022-06-09T00:38:58.862Z][L]: -------------------------
// [2022-06-09T00:38:58.865Z][L]: Log .log
// [2022-06-09T00:38:58.866Z][L]: Log .text
// [2022-06-09T00:38:58.869Z][L]: -------------------------
// ----- [GROUPEND]: Group log -----

await logger.line();
await logger.delimiter(); // alias
await logger.separator(); // alias
// [2022-06-09T00:38:58.871Z][L]: -------------------------
// [2022-06-09T00:38:58.872Z][L]: -------------------------
// [2022-06-09T00:38:58.874Z][L]: -------------------------

await logger.empty();
await logger.blank();
await logger.null();
//
//
//

await logger.log('Log .log');
await logger.logging('Log .log'); // alias
await logger.text('Log .log'); // alias
await logger.l('Log .log'); // alias
// [2022-06-09T00:38:58.881Z][L]: Log .log
// [2022-06-09T00:38:58.882Z][L]: Log .log
// [2022-06-09T00:38:58.884Z][L]: Log .log
// [2022-06-09T00:38:58.885Z][L]: Log .log

await logger.info('Log .info');
await logger.information('Log .info'); // alias
await logger.i('Log .info'); // alias
// [2022-06-09T00:38:58.887Z][I]: Log .info
// [2022-06-09T00:38:58.888Z][I]: Log .info
// [2022-06-09T00:38:58.889Z][I]: Log .info

await logger.error('Log .error');
await logger.err('Log .error');
await logger.e('Log .error');
// [2022-06-09T00:38:58.890Z][E]: Log .error
// [2022-06-09T00:38:58.928Z][E]: Log .error
// [2022-06-09T00:38:58.931Z][E]: Log .error

const json = {
  strict: { column: { age: 24, name: 'Yury', job: 'Developer' } },
}; // logger.dev, logger.json
await logger.dev('Log .dev'); // No record log file
await logger.developer('Log .dev'); // No record log file / alias
await logger.d('Log .dev'); // No record log file / alias
// [2022-06-09T00:38:58.933Z][D]: Log .dev
// [2022-06-09T00:38:58.933Z][D]: Log .dev
// [2022-06-09T00:38:58.934Z][D]: Log .dev

await logger.dev(json, true); // true - on mode JSON.STRINGIFY / No record log file
await logger.developer(json, true); // true - on mode JSON.STRINGIFY / No record log file / alias
await logger.d(json, true); // true - on mode JSON.STRINGIFY / No record log file / alias
// [2022-06-09T00:38:58.934Z][D]: {
//     "strict": {
//             "column": {
//                     "age": 24,
//                     "name": "Yury",
//                     "job": "Developer"
//             }
//     }
// }
// [2022-06-09T00:38:58.936Z][D]: {
//     "strict": {
//             "column": {
//                     "age": 24,
//                     "name": "Yury",
//                     "job": "Developer"
//             }
//     }
// }
// [2022-06-09T00:38:58.938Z][D]: {
//     "strict": {
//             "column": {
//                     "age": 24,
//                     "name": "Yury",
//                     "job": "Developer"
//             }
//     }
// }
// [2022-06-09T00:38:58.940Z][JSON]: {
//     "strict": {
//             "column": {
//                     "age": 24,
//                     "name": "Yury",
//                     "job": "Developer"
//             }
//     }
// }

await logger.json(json); // No record log file
await logger.stringify(json); // No record log file / alias
// [2022-06-09T00:38:58.940Z][JSON]: {
//     "strict": {
//             "column": {
//                     "age": 24,
//                     "name": "Yury",
//                     "job": "Developer"
//             }
//     }
// }
// [2022-06-09T00:38:58.942Z][JSON]: {
//     "strict": {
//             "column": {
//                     "age": 24,
//                     "name": "Yury",
//                     "job": "Developer"
//             }
//     }
// }
```

### Change Log

#### v1.1.1

```javascript
logger.group('group alias');
logger.delimiter(); // alias line
logger.separator(); // alias line
logger.blank(); // alias empty
logger.null(); // alias empty
logger.space(); // alias empty
logger.logging('log alias'); // alias log
logger.text('log alias'); // alias log
logger.l('log alias'); // alias log
logger.information('info alias'); // alias info
logger.i('info alias'); // alias info
logger.err('error alias'); // alias error
logger.e('error alias'); // alias error
logger.developer(json, true); // alias dev
logger.stringify(json); // alias json
logger.groupEnd('group alias');
//-- logger.groupend('group alias') //alias groupEnd
// ----- [group]: group alias -----
//   [2021-08-18T17:45:10.304Z][L]: -------------------------
//   [2021-08-18T17:45:10.305Z][L]: -------------------------
//
//
//
//   [2021-08-18T17:45:10.307Z][L]: log alias
//   [2021-08-18T17:45:10.307Z][L]: log alias
//   [2021-08-18T17:45:10.308Z][L]: log alias
//   [2021-08-18T17:45:10.308Z][I]: info alias
//   [2021-08-18T17:45:10.309Z][I]: info alias
//   [2021-08-18T17:45:10.309Z][E]: error alias
//   [2021-08-18T17:45:10.309Z][E]: error alias
//   [2021-08-18T17:45:10.310Z][D]: {
//         "strict": {
//                 "column": {
//                         "age": 24,
//                         "name": "Yury",
//                         "job": "Developer"
//                 }
//         }
//   }
//   [2021-08-18T17:45:10.312Z][D]: {
//         "strict": {
//                 "column": {
//                         "age": 24,
//                         "name": "Yury",
//                         "job": "Developer"
//                 }
//         }
//   }
//   [2021-08-18T17:45:10.314Z][JSON]: {
//         "strict": {
//                 "column": {
//                         "age": 24,
//                         "name": "Yury",
//                         "job": "Developer"
//                 }
//         }
//   }
//   ----- [groupEnd]: group alias -----
```

## Attention! Notice

```
You use this module at your own risk.
This module is delivered "as is".
```
